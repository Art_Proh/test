import java.util.Scanner;

import static java.lang.Math.*;

/**
 * Created by Artem Prokharau on 21.09.2020
 */
/*
- Вычислить длину окружности и площадь круга одного и того же заданного радиуса R.
 */

public class LengthAndArea {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        double radius = enterRadiusAndCheckValue(scanner);

        System.out.printf("Длина окружности равна => %.4f\n", lengthCircle(radius));// оставляем 4цифры после запятой
        System.out.printf("Площадь круга равна => %.2f", areaCircle(radius));// оставляем 2цифры после запятой

    }

    static double enterRadiusAndCheckValue(Scanner scanner) {
        double value;
        do {
            System.out.println("Введите значение r (r > 0)");
            while (!scanner.hasNextInt()) {
                System.out.println("Введите число!");
                scanner.next();
            }
            value = scanner.nextInt();
        } while (value < 0);
        return value;
    }

    static double areaCircle(double radius) {
        double area;
        area = PI * pow(radius, 2);
        return area;
    }

    static double lengthCircle(double radius) {
        double length;
        length = 2 * PI * radius;
        return length;
    }
}
