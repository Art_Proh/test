import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by Artem Prokharau on 21.09.2020
 */
/*
- Ввести с консоли а, b и с. Если нельзя построить треугольник с такими длинами сторон, то напечатать Ошибку,
иначе напечатать в зависимости от того, равносторонний это треугольник, равнобедренный или какой-либо иной
 */

public class Triangle {
    public static void main(String[] args) {


        Scanner sc = new Scanner(System.in);

        double side1 = enterSide(sc, "Введите длинну стороны a");

        double side2 = enterSide(sc, "Введите длинну стороны b");

        double side3 = enterSide(sc, "Введите длинну стороны c");


        if (checkTriangleVariant(side1, side2, side3)) {

            if (equilateralTriangle(side1, side2, side3)) {
                System.out.println("У вас равносторонний треугольник");

            } else if (isoscelesTriangle(side1, side2, side3)) {
                System.out.println("У вас равнобедренный треугольник");

            } else if (versatileTriangle(side1, side2, side3)) {
                System.out.println("У вас разносторонний треугольник");
            }

        } else
            System.out.println("Не существует такого треугольника");
    }

    static double enterSide(Scanner sc, String s) {
        double side;
        System.out.println(s);
        side = sc.nextInt();
        return side;
    }

    static boolean versatileTriangle(double side1, double side2, double side3) {
        return side1 != side2 && side2 != side3;
    }

    static boolean isoscelesTriangle(double side1, double side2, double side3) {
        return side1 == side2 || side1 == side3 || side2 == side3;
    }

    static boolean equilateralTriangle(double side1, double side2, double side3) {
        return side1 == side2 && side2 == side3;
    }

    static boolean checkTriangleVariant(double side1, double side2, double side3) {
        double[] max = {side1, side2, side3};
        Arrays.sort(max);
        return (max[0] + max[1]) > max[2];
    }

}
