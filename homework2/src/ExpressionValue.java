import java.util.Scanner;


/**
 * Created by Artem Prokharau on 20.09.2020
 */
/*
- Вычислить  значение  выражения  по  формуле  (все  переменные  принимают действительные значения)
x ln x + y/cos(x) - x/3
 */

public class ExpressionValue {
    public static void main(String[] args) {


        Scanner sc = new Scanner(System.in);

        double x = checkEnterValue(sc, "Введите значение х (х > 0)");

        double y = checkEnterValue(sc, "Введите значение y");


        double count = equationSolution(x, y);

        System.out.println("x*ln(x) + y / (cos(x) - x/3) = " + count);

    }

    static double checkEnterValue(Scanner scanner, String s) {
        double value;
        do {
            System.out.println(s);
            while (!scanner.hasNextInt()) {
                System.out.println("Введите число!");
                scanner.next();
            }
            value = scanner.nextInt();
        } while (value <= 0);
        return value;

    }

    static double equationSolution(double x, double y) {
        return x * Math.log(x) + y / (Math.cos(x) - x / 3);
    }

}
