import java.util.ArrayList;

/**
 * Created by Artem Prokharau on 23.09.2020
 */
/*
- Создать List любого типа, добавить в него 10 элементов (имена игроков, названия планет...),
удалить элемент по номеру, удалить элемент по значению, удалить все элементы, распечать рамер до и после, распечатать
 */

public class ListDemonstration {
    public static void main(String[] args) {

        ArrayList<String> color = new ArrayList<>();

        addColorsToArrayList(color);

        color.remove(3);
        color.remove((char) 27 + "[33m" + "yellow");

        System.out.println(color);

        color.clear();
        System.out.println(color + "\nКолличество элементов => " + color.size());


    }

    static void addColorsToArrayList(ArrayList<String> color) {
        color.add((char) 27 + "[33m" + "yellow");
        color.add((char) 27 + "[34m" + "blue");
        color.add((char) 27 + "[32m" + "green");
        color.add((char) 27 + "[37m" + "black");
        color.add((char) 27 + "[30m" + "white");
        color.add((char) 27 + "[31m" + "red");
        color.add((char) 27 + "[35m" + "purple");
        System.out.println(color + "\nКолличество элементов => " + color.size());

    }
}
