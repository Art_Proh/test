import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by Artem Prokharau on 24.09.2020
 */
/*
- Map. Создать HashMap<Integer, String>, заполнить даными, найти значение по ключу,
удалить по ключу, удалить по значению, распечать всё создержимое, заменить значение по существующему ключу
 */
public class MapDemonstration {

    public static void main(String[] args) {

        HashMap<Integer, String> map = new HashMap<>();

        map.put(1, "audi");
        map.put(2, "bmw");
        map.put(3, "toyota");
        map.put(4, "lexus");
        map.put(5, "mazda");
        map.put(6, "audi");

        System.out.println(map);
        System.out.println("Значение с ключом 2 => " + map.get(2));

        System.out.println("Удалено значение с ключом 5 => " + map.remove(5));

        removeElementsFromMap(map, "audi");


        System.out.println("\nЗамена сзначени с ключом 3 => " + map.replace(3, "opel") + " на opel");
        System.out.println(map);

    }

    static void removeElementsFromMap(HashMap<Integer, String> map, String value) {
        System.out.println("\nУдаляем значение => " + value + " из HashMap");

        if (map.containsValue(value)) {

            Iterator<Map.Entry<Integer, String>> iter = map.entrySet().iterator();

            while (iter.hasNext()) {

                Map.Entry<Integer, String> entry = iter.next();
                if (entry.getValue().equals(value)) {
                    iter.remove();
                }
            }
            System.out.println(map);
        } else {
            System.out.println("Такого значения нет в HashMap");
        }
    }
}
