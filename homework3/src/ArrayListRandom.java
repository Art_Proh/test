import java.util.ArrayList;

/**
 * Created by Artem Prokharau on 23.09.2020
 */
/*
- Создать List Integer, заполнить случайными значениями. распечать только чётные, только нечетные
 */

public class ArrayListRandom {
    public static void main(String[] args) {

        ArrayList<Integer> list = new ArrayList<>();

        addRandomValues(list);

        getEvenNumbers(list);
        getOddNumbers(list);

    }

    static void getOddNumbers(ArrayList<Integer> list) {
        System.out.print("\nНечетные значения => ");
        for (int i = 0; i < list.size(); i++) {
            int odd = list.get(i) % 2;
            if (odd == 1) {
                System.out.print(list.get(i) + ",");
            }
        }
    }

    static void getEvenNumbers(ArrayList<Integer> list) {
        System.out.print("Четные значения => ");
        for (int i = 0; i < list.size(); i++) {
            int even = list.get(i) % 2;
            if (even == 0) {
                System.out.print(list.get(i) + ",");
            }
        }
    }

    static void addRandomValues(ArrayList<Integer> list) {
        for (int i = 0; i < 10; i++) {
            list.add((int) (Math.random() * 100));
        }
        System.out.println("Значения в Arraylist => " + list);
    }
}
