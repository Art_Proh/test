import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Artem Prokharau on 24.09.2020
 */
/*
- Создать и наполнить List целочисленный, отсортировать и
распечатть по возрастани, по убыванию, добавить несколько одинаковых значений, удалить дубликаты
 */
public class SortedList {
    public static void main(String[] args) {

        ArrayList<Integer> list = new ArrayList<>();


        addRandomValues(list);

        sortAscending(list);//по возрастанию
        sortDescending(list);//по убыванию

        removeDuplicate(list);

    }

    static void addRandomValues(ArrayList<Integer> list) {
        for (int i = 0; i < 10; i++) {
            list.add((int) (Math.random() * 100));
        }
        System.out.println(list + "\nКоличество элементов => " + list.size());
    }

    static void sortAscending(ArrayList<Integer> list) {
        Collections.sort(list);
        System.out.println("\nПо возрастанию => " + list);

    }

    static void sortDescending(ArrayList<Integer> list) {
        Collections.reverse(list);
        System.out.println("По убыванию => " + list);

    }

    static void removeDuplicate(ArrayList<Integer> list) {
        int sizeOldRemove = list.size();

        ArrayList<Integer> removeDuplicate = new ArrayList<>();
        for (Integer value : list) {
            if (!removeDuplicate.contains(value)) {
                removeDuplicate.add(value);
            }
        }
        int sizeAfterRemove = removeDuplicate.size();
        int quantityDuplicate = sizeOldRemove - sizeAfterRemove;

        System.out.println("\n" + removeDuplicate);
        System.out.println("Удалено дубликатов => " + quantityDuplicate);

    }
}
