package by.prohor;

import by.prohor.dao.TestDao;
import by.prohor.entity.sql.User;
import by.prohor.loader.Loader;
import by.prohor.parser.Jackson;

import java.util.Scanner;

/**
 * Created by Artem Prokharau on 28.10.2020
 */

public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        try {
            TestDao testDao = new TestDao();
            User user = createNewUserInTable(scanner, testDao);

            Loader loader = new Loader(testDao, scanner, user);

            selectTest();
            int selectUser = checkEnterValues(scanner);

            switch (selectUser) {
                case 1:
                    testDao.createTest("Автомобили", user);
                    loader.startTest(Jackson.loaderXmlFile());
                    break;
                case 2:
                    testDao.createTest("География", user);
                    loader.startTest(Jackson.loaderJsonFile());
                    break;
                case 3:
                    testDao.createTest("История", user);
                    loader.startTest(Jackson.loaderCsvFile());
                    break;
            }
            System.out.println(testDao.readUserAndTest(user));
        } catch (Exception e) {
            e.getStackTrace();
        }
    }


    public static User createNewUserInTable(Scanner scanner, TestDao testDao) {

        System.out.println("Здравствуйте. Введите свои данные: ");
        System.out.print("Ваше имя => ");
        String name = scanner.next();

        System.out.print("Ваша фамилия => ");
        String surName = scanner.next();

        System.out.print("Ваш логин => ");
        String login = checkLogin(scanner, testDao);

        User user = new User(name, surName, login);
        testDao.createUser(user);

        return user;
    }

    private static String checkLogin(Scanner scanner, TestDao testDao) {

        String checkLogin = scanner.next();
        while (testDao.checkLoginOnDataBase(checkLogin)) {
            System.out.println("пользователь существует.");
            checkLogin = scanner.next();
        }
        return checkLogin;
    }


    public static void selectTest() {
        System.out.println("\nВведите номер теста" +
                "\n1 => Автомобили" +
                "\n2 => География" +
                "\n3 => История");
    }

    public static int checkEnterValues(Scanner sc) {
        int selectUser;
        do {
            System.out.println("Введите число из списка!");
            while (!sc.hasNextInt()) {
                System.out.println("Введите число!");
                sc.next();
            }
            selectUser = sc.nextInt();
        } while (selectUser < 1 || selectUser > 3);
        return selectUser;
    }
}
