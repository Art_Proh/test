package by.prohor.loader;

import by.prohor.dao.TestDao;
import by.prohor.entity.sql.User;
import by.prohor.entity.test.Testing;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Artem Prokharau on 28.10.2020
 */

public class Loader {

    private final TestDao testDao;
    private final Scanner scanner;
    private final User user;


    public Loader(TestDao testDao, Scanner scanner, User user) {
        this.testDao = testDao;
        this.scanner = scanner;
        this.user = user;
    }

    public void startTest(Testing test) throws IOException, SQLException {


        test.setFalseAnswer(0);
        test.setTrueAnswer(0);

        for (int i = 0; i < test.getQuestions().size(); i++) {
            System.out.println(test.getQuestions().get(i));
            String selectUser = complianceCheck(test, i);
            if (test.getAnswers().get(i).equalsIgnoreCase(selectUser)) {
                System.out.println("Ответ правильный!");
                test.setTrueAnswer((test.getTrueAnswer() + 1));
            } else {
                System.out.println("Ответ неверный!  Правильный ответ => " + test.getAnswers().get(i));
                test.setFalseAnswer((test.getFalseAnswer() + 1));
            }
            System.out.println("-------------------------------");
        }

        System.out.println("Правильных ответов " + test.getTrueAnswer());
        System.out.println("Неправельных ответов " + test.getFalseAnswer());

        test.setResult((test.getTrueAnswer() * 100 / test.getAnswers().size()));
        System.out.println("Процент успеха равен => " + test.getResult());

        test.setNumberOfAttempts(test.getNumberOfAttempts() + 1);
        System.out.println("Колличество попыток => " + test.getNumberOfAttempts());
        testDao.updateResultTest(test, user);

        System.out.println("Хотите повторить?  да/нет  ");
        operatorExitOrReplay(test);
    }


    private void operatorExitOrReplay(Testing test) throws IOException, SQLException {

        String selectUser = scanner.next();

        if (selectUser.equalsIgnoreCase("да")) {
            startTest(test);
        } else if (selectUser.equalsIgnoreCase("нет")) {
            System.out.println("Спасибо за использование теста");
        } else {
            System.out.println("Введите \"да\" или \"нет\"");
            operatorExitOrReplay(test);
        }
    }


    private String complianceCheck(Testing test, int i) {
        String selectUser = scanner.next();

        Pattern pattern = Pattern.compile("[(]([а-яА-Яa-zA-Z0-9]*)[)]", Pattern.UNICODE_CASE);
        Matcher matcher = pattern.matcher(test.getQuestions().get(i));
        while (matcher.find()) {
            if (StringUtils.containsIgnoreCase(selectUser, matcher.group(1))) {
                return selectUser;
            }
        }
        System.out.println("Ваш ответ \"" + selectUser + "\" не находится в списке предложенных");
        System.out.println("введите один из вариантов!");
        return complianceCheck(test, i);
    }
}

