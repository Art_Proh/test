package by.prohor.entity.sql;


/**
 * Created by Artem Prokharau on 05.11.2020
 */

public class User {

    private int id;
    private final String name;
    private final String surname;
    private final String login;
    private TestResult testResults;


    public User(String name, String surname, String login, TestResult testResults) {
        this.name = name;
        this.surname = surname;
        this.login = login;
        this.testResults = testResults;
    }

    public User(String name, String surname, String login) {
        this.name = name;
        this.surname = surname;
        this.login = login;
    }


    public String getLogin() {
        return login;
    }

    public String getName() {
        return name;
    }


    public String getSurname() {
        return surname;
    }


    @Override
    public String toString() {
        return "Ваши результаты => " +
                "Имя = '" + name + '\'' +
                ", Фамилия = '" + surname + '\''
                + testResults;
    }
}

