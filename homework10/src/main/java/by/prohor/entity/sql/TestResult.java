package by.prohor.entity.sql;

/**
 * Created by Artem Prokharau on 05.11.2020
 */

public class TestResult {

    private String typeTest;
    private Integer result;
    private Integer tryNumber;


    public TestResult(String typeTest, Integer result, Integer tryNumber) {
        this.typeTest = typeTest;
        this.result = result;
        this.tryNumber = tryNumber;
    }

    public TestResult(String typeTest) {
        this.typeTest = typeTest;
    }

    public Integer getTryNumber() {
        return tryNumber;
    }

    public void setTryNumber(Integer tryNumber) {
        this.tryNumber = tryNumber;
    }

    public String getTypeTest() {
        return typeTest;
    }

    public void setTypeTest(String typeTest) {
        this.typeTest = typeTest;
    }


    public Integer getResult() {
        return result;
    }

    public void setResult(Integer result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "\nТест = '" + typeTest + '\'' +
                ", результ = " + result +
                ", количество попыток = " + tryNumber;
    }
}
