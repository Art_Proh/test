package by.prohor.entity.test;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Artem Prokharau on 28.10.2020
 */

public class Testing {

    private ArrayList<String> questions;
    private ArrayList<String> answers;


    private int trueAnswer;
    private int falseAnswer;
    private int result;
    private int numberOfAttempts;

    public Testing() {
    }

    public List<String> getQuestions() {
        return questions;
    }

    public int getNumberOfAttempts() {
        return numberOfAttempts;
    }


    public void setNumberOfAttempts(int numberOfAttempts) {
        this.numberOfAttempts = numberOfAttempts;
    }

    public List<String> getAnswers() {
        return answers;
    }


    public int getTrueAnswer() {
        return trueAnswer;
    }


    public void setTrueAnswer(int trueAnswer) {
        this.trueAnswer = trueAnswer;
    }

    public int getFalseAnswer() {
        return falseAnswer;
    }


    public void setFalseAnswer(int falseAnswer) {
        this.falseAnswer = falseAnswer;
    }

    public int getResult() {
        return result;
    }


    public void setResult(int result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "Тест" +
                "questions=" + questions +
                ", answering=" + answers +
                ", trueAnswer=" + trueAnswer +
                ", falseAnswer=" + falseAnswer +
                ", result=" + result +
                '}';
    }
}
