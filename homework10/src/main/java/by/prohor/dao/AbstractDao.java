package by.prohor.dao;

import by.prohor.entity.sql.User;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by Artem Prokharau  05.11.2020
 */

public abstract class AbstractDao implements ResultDao<User> {

    private final String login = "prohor";
    private final String psw = "art375pro";
    private final String url = "jdbc:mysql://localhost:3306/prohorArt";


    public Connection getConnection() throws SQLException {
        return DriverManager.getConnection(url, login, psw);
    }


}
