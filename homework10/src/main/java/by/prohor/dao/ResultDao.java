package by.prohor.dao;

import by.prohor.entity.test.Testing;

/**
 * Created by Artem Prokharau on 05.11.2020
 */

public interface ResultDao<T> {

    void createUser(T obj);

    void createTest(String test, T obj);

    T readUserAndTest(T obj);

    void updateResultTest(Testing test, T obj);

    Boolean checkLoginOnDataBase(String login);
}
