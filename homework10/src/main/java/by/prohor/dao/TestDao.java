package by.prohor.dao;


import by.prohor.entity.sql.TestResult;
import by.prohor.entity.sql.User;
import by.prohor.entity.test.Testing;
import by.prohor.logger.LoggerUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by Artem Prokharau on 05.11.2020
 */

public class TestDao extends AbstractDao {

public static final Logger logger = LoggerFactory.getLogger(LoggerUtils.getCurrentClassName());

    @Override
    public void createUser(User user) {

        String requestSql = "INSERT INTO prohorArt.user (login,name,surname)  VALUES (?,?,?)";


        try (PreparedStatement statement = getConnection().prepareStatement(requestSql)) {
            logger.debug("Подключился к базе и отправил запрос для регистрации пользователя: {}",statement);

            statement.setString(1, user.getLogin());
            logger.trace("запись логина: {}",user.getLogin());
            statement.setString(2, user.getName());
            logger.trace("запись имени: {}",user.getName());
            statement.setString(3, user.getSurname());
            logger.trace("запись фамилии: {}",user.getSurname());
            statement.executeUpdate();
            logger.debug("Данные добавленны в базу");

        } catch (SQLException ex) {
            logger.error(ex.getMessage(),ex);
            ex.printStackTrace();
        }
    }


    @Override
    public void createTest(String test, User user) {

        String requestSql = "INSERT INTO prohorArt.test (user_login, type_test) VALUES (?,?)";

        try (PreparedStatement statement = getConnection().prepareStatement(requestSql)) {
            logger.debug("Подключился к базе и отправил запрос для создания теста: {}",statement);

            statement.setString(1, user.getLogin());
            logger.trace("запись логина: '{}' в таблицу",user.getLogin());
            statement.setString(2, test);
            logger.trace("запись теста с названием: {}",test);
            statement.executeUpdate();
            logger.debug("Данные добавленны в базу");

        } catch (SQLException ex) {
            logger.error(ex.getMessage(),ex);
            ex.printStackTrace();
        }
    }


    @Override
    public User readUserAndTest(User user) {

        String requestSql = "SELECT name,surname,login,type_test,result,try_number,data_complete FROM prohorArt.user u " +
                "INNER JOIN prohorArt.test AS t ON t.user_login = u.login " +
                "WHERE u.login = '" + user.getLogin() + "'";


        try (Statement statement = getConnection().createStatement()) {
            logger.debug("Подключился к базе: {}",statement);


            ResultSet resultSet = statement.executeQuery(requestSql);
            logger.debug("Чтение данных: {}",resultSet);


            while (resultSet.next()) {
                String name = resultSet.getString("name");
                logger.trace("получение имени из базы: {}",name);
                String surname = resultSet.getString("surname");
                logger.trace("получение фамилии из базы: {}",surname);
                String login = resultSet.getString("login");
                logger.trace("получение логина из базы: {}",login);
                String typeTest = resultSet.getString("type_test");
                logger.trace("получение названия теста из базы: {}",typeTest);
                Integer result = resultSet.getInt("result");
                logger.trace("получение результата теста из базы: {}",result);
                int tryNumber = resultSet.getInt("try_number");
                logger.trace("получение количества попыток из базы: {}",tryNumber);

                user = new User(name, surname, login, new TestResult(typeTest, result, tryNumber));
                logger.debug("Создание пользователя с текщими данными: {}",user);
            }
        } catch (SQLException ex) {
            logger.error(ex.getMessage(),ex);
            ex.printStackTrace();
        }
        return user;
    }


    @Override
    public void updateResultTest(Testing test, User user) {

        String requestSql = "UPDATE prohorArt.test  SET result = ? ,try_number = ? " +
                "WHERE user_login ='" + user.getLogin() + "'";

        try (PreparedStatement statement = getConnection().prepareStatement(requestSql)) {
            logger.debug("Подключился к базе для обнавления результатов теста: {}",statement);

            statement.setInt(1, test.getResult());
            logger.trace("добавление в таблицу результат теста: {}",test.getResult());
            statement.setInt(2, test.getNumberOfAttempts());
            logger.trace("добавление в таблицу количества попыток: {}",test.getNumberOfAttempts());
            statement.executeUpdate();
            logger.debug("Данные в базе обновленны");

        } catch (SQLException ex) {
            logger.error(ex.getMessage(),ex);
            ex.printStackTrace();
        }
    }

    @Override
    public Boolean checkLoginOnDataBase(String login) {

        boolean check = false;
        String requestSql = "SELECT login FROM prohorArt.user where login LIKE '" + login + "'";


        try (Statement statement = getConnection().createStatement()) {
            logger.debug("Подключился к базе: {}",statement);

            ResultSet resultSet = statement.executeQuery(requestSql);
            logger.debug("Чтение данных: {}",resultSet);


            while (resultSet.next()) {
                check = true;
                logger.info("В базе уже есть пользователь с таким логином => {}", resultSet.getString("login"));
            }
        } catch (SQLException ex) {
            logger.error(ex.getMessage(),ex);
            ex.printStackTrace();
        }
        return check;
    }
}
