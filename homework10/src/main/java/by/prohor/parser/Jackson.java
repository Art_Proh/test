package by.prohor.parser;


import by.prohor.entity.test.Testing;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Artem Prokharau on 28.10.2020
 */

public class Jackson {


    public static Testing loaderXmlFile() throws IOException {
        XmlMapper xmlMapper = new XmlMapper();
        ClassLoader classLoader = Jackson.class.getClassLoader();
        InputStream ir = classLoader.getResourceAsStream("car.xml");
        return xmlMapper.readValue(ir, Testing.class);
    }

    public static Testing loaderJsonFile() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        ClassLoader classLoader = Jackson.class.getClassLoader();
        InputStream ir = classLoader.getResourceAsStream("geography.json");
        return objectMapper.readValue(ir, Testing.class);

    }

    public static Testing loaderCsvFile() throws IOException {
        CsvMapper csvMapper = new CsvMapper();
        CsvSchema schema = CsvSchema.emptySchema().withHeader();
        ClassLoader classLoader = Jackson.class.getClassLoader();
        InputStream ir = classLoader.getResourceAsStream("history.csv");
        ObjectReader objectReader = csvMapper.readerFor(Testing.class).with(schema);
        return objectReader.readValue(ir, Testing.class);
    }
}





