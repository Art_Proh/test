package taskstring;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Artem Prokharau on 04.10.2020
 */

public class Task10FindSubstring {
    public static void main(String[] args) {

        Pattern pat = Pattern.compile("\\s*Java\\s+\\d*");
        Matcher mat = pat.matcher("Versions: Java  5, Java 6, Java   7, Java 8, Java 12. Java88");

        while (mat.find()) {
            System.out.println(mat.group().replaceAll("[\\s]+", " "));//выводим + убираем лишние пробелы
        }
    }

//    "fffff ab f 1234 jkjk"

//Найти слово, в котором число различных символов минимально. Слово может содержать буквы и цифры.
// Если таких слов несколько, найти первое из них. Например в строке "fffff ab f 1234 jkjk" найденное слово должно быть "fffff".
}
