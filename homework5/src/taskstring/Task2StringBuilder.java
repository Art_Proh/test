package taskstring;

/**
 * Created by Artem Prokharau on 30.09.2020
 */

public class Task2StringBuilder {
    public static void main(String[] args) {

        StringBuilder sb = new StringBuilder();
/*
            выражение 3 + 56 = 59
 */
        sb.append(3);
        sb.append(" + ");
        sb.append(56);
        sb.append(" = ");
        sb.append(3+56);
        System.out.println(sb);

/*
             выражение 3 - 56 = -53
 */
        sb.setCharAt(sb.indexOf("+"), '-');
        sb.replace(sb.indexOf("59"), sb.length(), "-53");

        System.out.println(sb);

/*
             выражение 3 * 56 = 168

 */
        sb.setCharAt(sb.indexOf("-"), '*');
        sb.replace(sb.indexOf("-53"), sb.length(), "168");

        System.out.println(sb);

/*
             Замена символа " = " на слово "равно" 1й вариант
 */
        sb.insert(sb.indexOf("="), "равно");
        sb.deleteCharAt(sb.indexOf("="));
        System.out.println(sb);

 /*
              Замена " равно " на слово " equals " 2й вариант
 */
        sb.replace(sb.indexOf("равно"), sb.lastIndexOf(" 168"), "equals");
        System.out.println(sb);

    }

}
