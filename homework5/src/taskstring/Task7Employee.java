package taskstring;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Artem Prokharau on 04.10.2020
 */

public class Task7Employee {

    String fullname;
    double salary;

    public Task7Employee(String fullname, double salary) {
        this.fullname = fullname;
        this.salary = salary;
    }

    public static void main(String[] args) {
        List<Task7Employee> list = new ArrayList<>();
        list.add(new Task7Employee("Артем Прохоров", 1200.342));
        list.add(new Task7Employee("Дмитрий Иванов", 854.484854));
        list.add(new Task7Employee("Аркадий Семенович", 5432.34));
        list.add(new Task7Employee("Кристина Бойко", 4543.342567));

        for (Task7Employee employee : list) {
            Report.generateReport(employee.fullname, employee.salary);
        }
    }
}

class Report {

    public static void generateReport(String fullname, double salary) {
        System.out.printf("Сотрудник => %s \tПолучает зарплату в размере => %8.2f\n", fullname, salary);
    }
}

