package taskstring;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Artem Prokharau on 01.10.2020
 */

public class Task3ReplacementWord {

    public static final String FULL_NAME = "Object-oriented programming";
    public static final String ABBREVIATION = "OOP";
    public static final String INITIAL_TEXT = "Object-orienTed programming is a programming " +
            "language model organized Object-oriented programming around objects rather than" +
            "\"actions\" and data rather than logic. Object-oriented " +
            "programming blabla. object-orienTED programming bla." +
            "organized ObjEct-oriented programmIng around objects rather than" +
            "organized ObjeCt-oriEnteD programming around objects rather than";

    public static void main(String[] args) {

        replaceWordInText();

    }

    static void replaceWordInText() {

        StringBuilder sb = new StringBuilder(INITIAL_TEXT);

        List<Integer> arr = new ArrayList<>();

        Pattern pat = Pattern.compile(FULL_NAME, Pattern.CASE_INSENSITIVE);
        Matcher mat = pat.matcher(sb);

        while (mat.find()) {
            arr.add(mat.start());
        }

        for (int i = 1; i < arr.size(); i = i + 2) {
            if (i == 1) {
                sb.replace(arr.get(i), arr.get(i) + FULL_NAME.length(), ABBREVIATION);
            } else {
                replaceCompensation(arr, i, sb);//компенсация после первого удаления
                sb.replace(arr.get(i), arr.get(i) + FULL_NAME.length(), ABBREVIATION);
            }
        }
        System.out.println("Текст до замены => " + INITIAL_TEXT);
        System.out.println("Текст после замены => " + sb);
    }

    public static void replaceCompensation(List<Integer> arr, int i, StringBuilder sb) {
        arr.set(i, arr.get(i) - (INITIAL_TEXT.length() - sb.length()));
    }
}
