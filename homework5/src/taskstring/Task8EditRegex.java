package taskstring;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Artem Prokharau on 04.10.2020
 */

public class Task8EditRegex {
    public static void main(String[] args) {

        Pattern pat = Pattern.compile("\\s*c*ab\\s*c*ab\\s*c*ab\\s*");// Сразу для всей строки))
        Matcher mat = pat.matcher(" cab  ccab     cccab");
        boolean b = mat.matches();
        System.out.println(b);
    }
}
