package taskstring;

/**
 * Created by Artem Prokharau on 04.10.2020
 */

public class Task6Person {

    String name;
    int age;
    String say;

    public Task6Person(String name, int age, String say) {
        this.name = name;
        this.age = age;
        this.say = say;
    }

    @Override
    public String toString() {
        return String.format("Меня зовут %s и мне %d лет. %s", name, age, say);
    }
}

class Main {
    public static void main(String[] args) {

        Task6Person person = new Task6Person("Артем", 28, "Я хочу стать Java-разработчиком))");
        System.out.println(person);
    }
}
