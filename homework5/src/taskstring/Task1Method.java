package taskstring;

/**
 * Created by Artem Prokharau on 29.09.2020
 */

public class Task1Method {
    public static void main(String[] args) {

        printText("I like Java!!!");

    }

    static void printText(String txt) {
        System.out.println(txt);
        System.out.println("Последний символ строки => " + txt.charAt(txt.length() - 1));
        System.out.println("Строка заканчивается на '!!!' => " + txt.endsWith("!!!"));
        System.out.println("Строка начинается на 'I like' => " + txt.startsWith("I like"));
        System.out.println("Строка содержит слово 'Java' => " + txt.contains("Java"));
        System.out.println("На какой позиции строка содержит слово 'Java' => " + txt.indexOf("Java"));
        System.out.println("Меняем все символы 'a' на 'o' => " + txt.replace('a', 'o'));
        System.out.println("Только большие буквы(верхний регистр) => " + txt.toUpperCase());
        System.out.println("Только маленькие буквы(нижний регистр) => " + txt.toLowerCase());
        System.out.println("Вырезаем слово 'Java' из строки => " + txt.substring(txt.indexOf("Java"), txt.lastIndexOf("!!!")));

    }
}
