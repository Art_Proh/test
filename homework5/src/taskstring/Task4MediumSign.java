package taskstring;


import java.util.Scanner;

/**
 * Created by Artem Prokharau on 03.10.2020
 */

public class Task4MediumSign {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        returnMediumSign(sc);
    }

    static void returnMediumSign(Scanner scanner) {
        String str = "";
        do {
            System.out.println("Введите строку \"Длина четное число\"");
            while (scanner.hasNextInt()) {
                System.out.println("Введите букву");
                scanner.next();
            }
            str = scanner.next();
        } while (str.length() % 2 != 0);

        System.out.println(str.substring((str.length() / 2) - 1, (str.length() / 2) + 1));
    }
}



