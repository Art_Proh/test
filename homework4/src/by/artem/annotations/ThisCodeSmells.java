package by.artem.annotations;

import java.lang.annotation.Repeatable;

@Repeatable(AllThisCodeSmells.class)
public @interface ThisCodeSmells {
    String reviewer() default "user";
}

@interface AllThisCodeSmells {
    ThisCodeSmells[] value();
}
