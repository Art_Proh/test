package by.artem.transportpark.transport;

import by.artem.annotations.ProdCode;
import by.artem.transportpark.exceptions.transport.ProblemWithDriverException;
import by.artem.transportpark.service.Service;
import by.artem.transportpark.transport.fueltype.FuelType;
import by.artem.transportpark.workers.drivers.Drivers;

/**
 * Created by Artem Prokharau on 27.09.2020
 */


public abstract class Transport implements Service {

    private final int numberOfSeats;
    private final int numberOfStops;
    protected FuelType FUELTYPE;
    private final String typeTransport;


    public Transport(int numberOfSeats, int numberOfStops, String typeTransport) {
        this.numberOfSeats = numberOfSeats;
        this.numberOfStops = numberOfStops;
        this.typeTransport = typeTransport;
    }

    @ProdCode
    public void startTransport() {
        clear();
        maintenance();
        try {
            move();
        } catch (ProblemWithDriverException e) {
            System.err.println(e.getMessage());
        }
    }

    @Override
    public final void clear() {
        System.out.println("Прошел процесс мойки");
    }

    @Override
    public final void maintenance() {
        System.out.println("Прошел процесс обслуживания");
    }

    @Override
    public void move() throws ProblemWithDriverException {
        if (Drivers.testMedic | Drivers.testAlcohol) {
            throw new ProblemWithDriverException();
        } else {
            System.out.println("Транспорт вышел на маршрут");
        }
    }

    @Override
    public String toString() {
        return "Тип транспорта => " + typeTransport +
                "\nКоличество мест = " + numberOfSeats +
                "\nКоличество остановок = " + numberOfStops +
                "\nТип топлива => " + FUELTYPE + "\n";
    }
}

