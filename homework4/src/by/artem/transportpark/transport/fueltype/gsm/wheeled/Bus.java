package by.artem.transportpark.transport.fueltype.gsm.wheeled;

import by.artem.transportpark.service.Refueling;
import by.artem.transportpark.transport.Transport;
import by.artem.transportpark.transport.fueltype.FuelType;

/**
 * Created by Artem Prokharau on 27.09.2020
 */

public class Bus extends Transport implements Refueling {

    public Bus() {
        super(74, 21, "Автобус");
        FUELTYPE = FuelType.GSM;
    }

    @Override
    public void refuelingFullTank() {
        System.out.println("В автобусе заправлен полный бак");
    }
}


