package by.artem.transportpark.transport.fueltype.gsm.wheeled;

import by.artem.transportpark.service.Refueling;
import by.artem.transportpark.transport.Transport;
import by.artem.transportpark.transport.fueltype.FuelType;

/**
 * Created by Artem Prokharau on 27.09.2020
 */

public class RouteTaxi extends Transport implements Refueling {

    public RouteTaxi() {
        super(18, 54, "Маршрутное такси");
        FUELTYPE = FuelType.GSM;
    }

    @Override
    public void refuelingFullTank() {
        System.out.println("В маршрутном такси заправлен полный бак");
    }
}

