package by.artem.transportpark.transport.fueltype.gsm.water;

import by.artem.transportpark.service.Refueling;
import by.artem.transportpark.transport.Transport;
import by.artem.transportpark.transport.fueltype.FuelType;

/**
 * Created by Artem Prokharau on 27.09.2020
 */

public class Ferry extends Transport implements Refueling {

    public Ferry() {
        super(60, 2, "Паром");
        FUELTYPE = FuelType.GSM;
    }


    @Override
    public void refuelingFullTank() {
        System.out.println("В пароме заправлен полный бак");
    }
}
