package by.artem.transportpark.transport.fueltype.electro.rail;

import by.artem.transportpark.transport.Transport;
import by.artem.transportpark.transport.fueltype.FuelType;

/**
 * Created by Artem Prokharau on 27.09.2020
 */

public class Underground extends Transport {

    public Underground() {
        super(120, 25, "Метро");
        FUELTYPE = FuelType.ELECTRO;
    }

}
