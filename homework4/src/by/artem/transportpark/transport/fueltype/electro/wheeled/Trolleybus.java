package by.artem.transportpark.transport.fueltype.electro.wheeled;

import by.artem.transportpark.service.Service;
import by.artem.transportpark.transport.Transport;
import by.artem.transportpark.transport.fueltype.FuelType;

/**
 * Created by Artem Prokharau on 27.09.2020
 */

public class Trolleybus extends Transport implements Service {

    public Trolleybus() {
        super(36, 12, "Троллейбус");
        FUELTYPE = FuelType.ELECTRO;
    }

}
