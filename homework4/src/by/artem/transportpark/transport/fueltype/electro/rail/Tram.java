package by.artem.transportpark.transport.fueltype.electro.rail;

import by.artem.transportpark.transport.Transport;
import by.artem.transportpark.transport.fueltype.FuelType;

/**
 * Created by Artem Prokharau on 27.09.2020
 */

public class Tram extends Transport {

    public Tram() {
        super(50, 10, "Трамвай");
        FUELTYPE = FuelType.ELECTRO;
    }

}
