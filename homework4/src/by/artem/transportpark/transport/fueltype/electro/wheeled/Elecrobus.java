package by.artem.transportpark.transport.fueltype.electro.wheeled;

import by.artem.transportpark.service.Changer;
import by.artem.transportpark.transport.Transport;
import by.artem.transportpark.transport.fueltype.FuelType;

/**
 * Created by Artem Prokharau on 27.09.2020
 */

public class Elecrobus extends Transport implements Changer {

    public Elecrobus() {
        super(48, 12, "Электробус");
        FUELTYPE = FuelType.ELECTRO;
    }

    @Override
    public void changerAllBattery() {
        System.out.println("Электробус полностью заряжен и готов к использованию");
    }
}

