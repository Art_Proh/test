package by.artem.transportpark.service;

import by.artem.transportpark.exceptions.transport.ProblemWithDriverException;

/**
 * Created by Artem Prokharau on 27.09.2020
 */

public interface Service {

    void clear();

    void maintenance();

    void move() throws ProblemWithDriverException;
}
