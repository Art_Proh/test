package by.artem.transportpark.service;

/**
 * Created by Artem Prokharau on 27.09.2020
 */

public interface Changer {

    void changerAllBattery();
}
