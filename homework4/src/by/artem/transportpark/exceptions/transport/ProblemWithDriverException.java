package by.artem.transportpark.exceptions.transport;

/**
 * Created by Artem Prokharau on 15.10.2020
 */

public class ProblemWithDriverException extends Exception {

    private final String textException;

    public ProblemWithDriverException() {
        this.textException = "Транспорт не вышел на маршрут.Водитель не допущен к работе.";
    }

    @Override
    public String getMessage() {
        return textException;
    }
}
