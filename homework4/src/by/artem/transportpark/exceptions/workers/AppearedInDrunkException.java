package by.artem.transportpark.exceptions.workers;

/**
 * Created by Artem Prokharau on 15.10.2020
 */

public class AppearedInDrunkException extends Exception {

    private final String textException;

    public AppearedInDrunkException() {
        this.textException = "Зафиксированно появление в состоянии алкогольного опъянения";
    }

    @Override
    public String getMessage() {
        return textException;
    }
}
