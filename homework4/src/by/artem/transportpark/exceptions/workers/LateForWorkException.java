package by.artem.transportpark.exceptions.workers;

/**
 * Created by Artem Prokharau on 15.10.2020
 */

public class LateForWorkException extends RuntimeException {
    private final String textException;

    public LateForWorkException() {
        this.textException = "Зафиксированно опаздание на работу";
    }

    @Override
    public String getMessage() {
        return textException;
    }
}
