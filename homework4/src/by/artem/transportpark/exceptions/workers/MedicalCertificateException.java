package by.artem.transportpark.exceptions.workers;

/**
 * Created by Artem Prokharau on 15.10.2020
 */

public class MedicalCertificateException extends Exception {

    private final String textException;

    public MedicalCertificateException() {
        this.textException = "Просрочена медицинская справка!!";
    }

    @Override
    public String getMessage() {
        return textException;
    }
}
