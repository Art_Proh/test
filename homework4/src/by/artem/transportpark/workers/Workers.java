package by.artem.transportpark.workers;


import by.artem.transportpark.exceptions.workers.LateForWorkException;

/**
 * Created by Artem Prokharau on 11.10.2020
 */

public abstract class Workers {

    private final String name;
    private final String surname;
    private final String position;


    public Workers(String name, String surname, String typeWorkers) {
        this.name = name;
        this.surname = surname;
        this.position = typeWorkers;
    }


    public void startOfWorking(double timeStart) {
        try {
            if (timeStart > 8.00) {
                throw new LateForWorkException();
            }
            System.out.println("Работник пришел на работу вовремя. Все в порядке");
        } catch (LateForWorkException e) {
            System.err.println(e.getMessage());
        }
    }

    @Override
    public String toString() {
        return String.format("\nДолжность => %s" +
                "\nИмя => %2s" +
                "\nФамилия => %2s\n", this.position, this.name, this.surname);
    }
}