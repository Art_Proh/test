package by.artem.transportpark.workers.drivers;

import by.artem.annotations.ProdCode;
import by.artem.transportpark.exceptions.workers.AppearedInDrunkException;
import by.artem.transportpark.exceptions.workers.MedicalCertificateException;
import by.artem.transportpark.workers.Workers;

/**
 * Created by Artem Prokharau on 11.10.2020
 */

public class Drivers extends Workers {

    public static boolean testAlcohol = false;
    public static boolean testMedic = false;


    public Drivers(String name, String surname, String typeWorkers) {
        super(name, surname, typeWorkers);
    }


    @ProdCode
    private void startWork(double timeStart, double promileAlcohol, int certificateDate) {
        startOfWorking(timeStart);
        try {
            alcoholTest(promileAlcohol);
        } catch (AppearedInDrunkException e) {
            System.err.println(e.getMessage());
        }
        try {
            checkMedicalCertificate(certificateDate);
        } catch (MedicalCertificateException e) {
            System.err.println(e.getMessage());
        }
    }

    public void alcoholTest(double promileAlcohol) throws AppearedInDrunkException {
        if (promileAlcohol > 0.2) {
            testAlcohol = true;
            throw new AppearedInDrunkException();
        }
        System.out.println("Работник прошел тест на алкоголь. Все в порядке");
    }

    public void checkMedicalCertificate(int certificateDate) throws MedicalCertificateException {
        if (certificateDate > 3) {
            testMedic = true;
            throw new MedicalCertificateException();
        }
        System.out.println("Справка является годной. Работник допускается к работе");

    }

    @Override
    public String toString() {
        return super.toString();
    }
}

