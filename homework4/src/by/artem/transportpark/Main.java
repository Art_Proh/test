package by.artem.transportpark;

import by.artem.reflection.RemoteControl;
import by.artem.transportpark.transport.Transport;
import by.artem.transportpark.transport.fueltype.electro.rail.Tram;
import by.artem.transportpark.transport.fueltype.electro.rail.Underground;
import by.artem.transportpark.transport.fueltype.electro.wheeled.Elecrobus;
import by.artem.transportpark.transport.fueltype.electro.wheeled.Trolleybus;
import by.artem.transportpark.transport.fueltype.gsm.water.Ferry;
import by.artem.transportpark.transport.fueltype.gsm.water.MotorShip;
import by.artem.transportpark.transport.fueltype.gsm.wheeled.Bus;
import by.artem.transportpark.transport.fueltype.gsm.wheeled.RouteTaxi;
import by.artem.transportpark.workers.drivers.Drivers;

import java.util.HashMap;
import java.util.Scanner;

/**
 * Created by Artem Prokharau on 27.09.2020
 */

public class Main {
    public static void main(String[] args) {

/**
 * Допуски
 * Алкоголь: Промилей не более 0.2
 * Справка: Срок не более 3мес.
 * Время: Начало рабочего времени не поздее 8.00
 */
/*
Спроектировать объектную модель для заданной предметной области:
 */

//        Drivers drivers = new Drivers("Артем", "Прохоров", "Водитель");
//        System.out.println(drivers);
        Drivers driver = (Drivers) RemoteControl.createdDriver();
        System.out.println(driver);


//        driver.startWork(9.00, 0.2, 2);
        RemoteControl.startWork(8.00, 0.2, 5);


        Scanner sc = new Scanner(System.in);
        selectTransport();

        int selectUser = checkEnterValues(sc);
        HashMap<Integer, Transport> map = getTransportHashMap();

        System.out.println(map.get(selectUser));
        map.get(selectUser).startTransport();

    }

    public static HashMap<Integer, Transport> getTransportHashMap() {
        HashMap<Integer, Transport> map = new HashMap<>();
        map.put(1, new Ferry());
        map.put(2, new MotorShip());
        map.put(3, new Bus());
        map.put(4, new RouteTaxi());
        map.put(5, new Tram());
        map.put(6, new Underground());
        map.put(7, new Elecrobus());
        map.put(8, new Trolleybus());
        return map;
    }

    public static void selectTransport() {

        System.out.println("\nВведите номер транспорта по которому вам требуется информация" +
                "\n1 => Паром" +
                "\n2 => Теплоход" +
                "\n3 => Автобус" +
                "\n4 => Маршрутное такси" +
                "\n5 => Трамвай" +
                "\n6 => Метро" +
                "\n7 => Электробус" +
                "\n8 => Троллейбус");
    }


    public static int checkEnterValues(Scanner sc) {
        int selectUser;
        do {
            System.out.println("Введите число из списка!");
            while (!sc.hasNextInt()) {
                System.out.println("Введите число!");
                sc.next();
            }
            selectUser = sc.nextInt();
        } while (selectUser < 0 || selectUser > 8);
        return selectUser;
    }
}
