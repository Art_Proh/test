package by.artem.reflection;

import by.artem.annotations.ProdCode;
import by.artem.transportpark.transport.fueltype.electro.rail.Tram;

import java.lang.reflect.Method;

/**
 * Created by Artem Prokharau on 18.10.2020
 */

public class WorkOfProdCodeAnnotation {

    public static void main(String[] args) {

        try {
            Class<?> clazz = Class.forName("by.artem.transportpark.transport.fueltype.electro.rail.Tram");
            Tram tram = (Tram) clazz.getDeclaredConstructor().newInstance();

            Class<?> clazz2 = Class.forName("by.artem.transportpark.transport.Transport");
            Method toString = clazz2.getDeclaredMethod("toString");
            System.out.println(toString.invoke(tram));

            Method[] declaredMethods = clazz2.getDeclaredMethods();
            for (Method declaredMethod : declaredMethods) {
                ProdCode prodCode = declaredMethod.getAnnotation(ProdCode.class);
                if (prodCode != null) {
                    declaredMethod.invoke(tram);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
