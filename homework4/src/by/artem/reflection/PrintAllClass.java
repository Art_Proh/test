package by.artem.reflection;

import by.artem.transportpark.transport.Transport;
import by.artem.transportpark.transport.fueltype.electro.rail.Tram;
import by.artem.transportpark.transport.fueltype.electro.rail.Underground;
import by.artem.transportpark.transport.fueltype.electro.wheeled.Elecrobus;
import by.artem.transportpark.transport.fueltype.electro.wheeled.Trolleybus;
import by.artem.transportpark.transport.fueltype.gsm.water.Ferry;
import by.artem.transportpark.transport.fueltype.gsm.water.MotorShip;
import by.artem.transportpark.transport.fueltype.gsm.wheeled.Bus;
import by.artem.transportpark.transport.fueltype.gsm.wheeled.RouteTaxi;
import by.artem.transportpark.workers.Workers;
import by.artem.transportpark.workers.drivers.Drivers;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Artem Prokharau on 17.10.2020
 */

public class PrintAllClass {

    static String colorDefault = (char) 27 + "[38m";


    public static void main(String[] args) {

        ArrayList<Class<?>> listClass = new ArrayList<>();

        listClass.add(Transport.class);
        listClass.add(Ferry.class);
        listClass.add(MotorShip.class);
        listClass.add(Bus.class);
        listClass.add(RouteTaxi.class);
        listClass.add(Tram.class);
        listClass.add(Underground.class);
        listClass.add(Elecrobus.class);
        listClass.add(Trolleybus.class);
        listClass.add(Workers.class);
        listClass.add(Drivers.class);


        getClazzAndPrintModifications(listClass);


    }

    public static void getMethodsClass(Class<?> temp) {
        String colorMethod = (char) 27 + "[36m";
        Method[] methods = temp.getDeclaredMethods();
        System.out.println(colorDefault + "Методы: Кол-во => " + methods.length);
        for (Method method : methods) {
            System.out.println("Имя => " + colorMethod + method.getName() + colorDefault + "\nПараметры => "
                    + colorMethod + Arrays.toString(method.getParameterTypes()) + colorDefault);
        }
        System.out.println("\n");
    }


    public static void getConstructorClass(Class<?> temp) {
        String colorConstructor = (char) 27 + "[34m";
        Constructor<?>[] constructors = temp.getDeclaredConstructors();
        System.out.println(colorDefault + "Конструкторы: Кол-во => " + constructors.length);
        for (Constructor<?> constructor : constructors) {
            System.out.println("Имя => " + colorConstructor + constructor.getName() + colorDefault + "\nПараметры => "
                    + colorConstructor + Arrays.toString(constructor.getParameterTypes()) + colorDefault);
        }
        System.out.println("----------------------------------------");
    }


    public static void getFieldClass(Class<?> temp) {
        String colorField = (char) 27 + "[33m";
        Field[] declaredFields = temp.getDeclaredFields();
        System.out.println("Поля класса: Кол-во => " + declaredFields.length);
        for (Field field : declaredFields) {
            System.out.println("Тип => " + colorField + field.getType()
                    + colorDefault + "\nИмя поля => " + colorField + field.getName() + colorDefault);
        }
        System.out.println("----------------------------------------");
    }


    public static void getClazzAndPrintModifications(ArrayList<Class<?>> list) {
        String colorClass = (char) 27 + "[31m";
        for (Class<?> temp : list) {
            System.out.println("Имя класса => " + colorClass + temp.getSimpleName() + colorDefault);
            getFieldClass(temp);
            getConstructorClass(temp);
            getMethodsClass(temp);
        }

    }
}
