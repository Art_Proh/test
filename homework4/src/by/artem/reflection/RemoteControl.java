package by.artem.reflection;

import by.artem.transportpark.workers.drivers.Drivers;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

/**
 * Created by Artem Prokharau on 16.10.2020
 */

public class RemoteControl {


    public static Object createdDriver() {

        Class<?> clazz = Drivers.class;
        try {
            Constructor<?> constructor = clazz.getDeclaredConstructor(String.class, String.class, String.class);
            return constructor.newInstance("Артем", "Прохоров", "Водитель");
        } catch (Exception e) {
            return e.getMessage();
        }
    }


    public static void startWork(double time, double promile, int spravka) {

        try {
            Class<?> clazz = Class.forName("by.artem.transportpark.workers.drivers.Drivers");
            Method startWork = clazz.getDeclaredMethod("startWork", double.class, double.class, int.class);
            startWork.setAccessible(true);
            startWork.invoke(createdDriver(), time, promile, spravka);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
