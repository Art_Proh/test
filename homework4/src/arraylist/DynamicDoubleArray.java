package arraylist;

import java.util.Arrays;

/**
 * Created by Artem Prokharau on 08.10.2020
 */
/*
Создать собственный класс DynamicDoubleArray, содержащий внутри себя массив double
[], поддерживающий добавление элемента в конец массива (add), получение элемента по
индексу (get), печать внутреннего состояния (toString), удаление произвольного элемента
по индексу (remove), а также возможность задать начальный размер при вызове
конструктора. При изменении количества элементов внутренний массив должен
пересоздаваться.
 */

public class DynamicDoubleArray<E> {


    private Object[] array;
    private int count;


    public DynamicDoubleArray() {
        int defaultSize = 5;
        this.array = new Object[defaultSize];
        this.count = 0;
    }

    public DynamicDoubleArray(int maxSize) {
        this.array = new Object[maxSize];
        this.count = 0;
    }


    public void add(E element) {
        if (count == array.length) {
            array = Arrays.copyOf(array, array.length + 1);
        }
        array[count++] = element;
    }


    public int size() {
        return count;
    }


    public E get(int index) {
        return (E) array[index];
    }


    public void remove(int index) {
        try {
            Object check = array[index];
        } catch (IndexOutOfBoundsException e) {
            System.out.println("Нет элемента с таким индексом!!");
            return;
        }

        int nextIndex = index;

        for (int i = index; i < count - 1; i++) {
            array[i] = array[++nextIndex];
        }
        count--;
        array = Arrays.copyOf(array, count);
    }


    @Override
    public String toString() {

        if (array.length == 0) {
            return "[]";
        }
        StringBuilder sb = new StringBuilder("doubleArray => [ ");

        for (int i = 0; i < count; i++) {
            sb.append(array[i]).append(", ");
        }
        sb.append("]");

        return sb.toString();
    }
}


class Main {

    public static void main(String[] args) {

        DynamicDoubleArray<Double> arr = new DynamicDoubleArray<>();

        arr.add(5.5);
        arr.add(5.54);
        arr.add(34.5676);
        arr.add(12.5);
        arr.add(456.8);
        arr.add(767456.8);
        arr.add(4534.345);
        arr.add(343.5454);

//        DynamicDoubleArray<String> arr = new DynamicDoubleArray<>(5);
//
//        arr.add("Cat");
//        arr.add("Dog");
//        arr.add("Penguin");
//        arr.add("Rabbit");
//        arr.add("Horse");


        try {
            System.out.println(arr.get(2).getClass());
            System.out.println("Элемент под индексом 3 => " + arr.get(3));
        } catch (IndexOutOfBoundsException index) {
            System.err.println("Нет элемента с таким индексом");
        } catch (NullPointerException nullPoint) {
            System.err.println("Под индексом 2 значение 'null'");
        }

        System.out.println("---------------------------------");
        System.out.println(arr);
        System.out.println("Размер массива => " + arr.size());
        System.out.println("Удаляем элемент под индексом 4");
        arr.remove(4);
        System.out.println(arr);
        System.out.println("Размер массива => " + arr.size());

        System.out.println("---------------------------------");
        arr.remove(12);
        System.out.println("---------------------------------");

        System.out.println(arr);
        System.out.println("Размер массива => " + arr.size());
        System.out.println("Добавляем новый элемент => '543.454'");
        arr.add(543.454);
//        arr.add("Leon");
        System.out.println(arr);
        System.out.println("Размер массива => " + arr.size());
    }
}
