/**
 * Created by Artem Prokharau on 17.09.2020
 */

/*
- Выведите на консоль ваши ФИО, адрес и телефон.
 */

public class NameAndNumber {
    public static void main(String[] args) {
        String name = "Прохоров Артем Олегович";
        String address = "г.Брест ул.Покровская 8";
        String phone = "tel.+375 33 607-07-40";


        System.out.printf("%s\n%s\n%s", name, address, phone);
    }
}
