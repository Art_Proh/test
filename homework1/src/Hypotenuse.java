/**
 * Created by Artem Prokharau on 20.09.2020
 */

/*
- Напишите  программу  нахождения  гипотенузы  и  площади  прямоугольного треугольника по двум катетам.
 */

public class Hypotenuse {
    public static void main(String[] args) {
        double kat1 = 3;
        double kat2 = 4;

        System.out.println(result(kat1, kat2));

    }

    static double result(double a, double b) {
        double hypotenuse;
        hypotenuse = Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2));
        return hypotenuse;
    }
}
