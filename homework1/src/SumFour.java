/**
 * Created by Artem Prokharau on 20.09.2020
 */
/*
- Напишите программу вычисления суммы четырёх слагаемых.
 */
public class SumFour {
    public static void main(String[] args) {

        int[]arr = new int[4];
        int sum = 0;

        for (int i = 0; i < 4; i++) {
            arr[i] = (int) (Math.random() * 10);// Случайные значения в промежутке от 0 до 9
            System.out.print(arr[i] + ",");
        }

        for (int i = 0; i < arr.length; i++) {
            sum += arr[i];
        }

        System.out.println("\nСумма всех слагаемых = "+ sum);
    }
}
