package by.prohor.entity.employee;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Created by Artem Prokharau on 15.11.2020
 */

@Component
//@Builder
//@Entity
public class EmployeeImpl implements Employee {

    //    @Id
    private int id;
    @Value("${name}")
    private String name;
    @Value("${surname}")
    private String surname;
    @Value("${login}")
    private String login;
    @Value("${fullYears}")
    private Integer fullYears;
    @Value("${servedInTheArmy}")
    private Boolean servedInTheArmy;

    public EmployeeImpl() {
    }

    public EmployeeImpl(String name, String surname, String login, Integer fullYears, Boolean servedInTheArmy) {
        this.name = name;
        this.surname = surname;
        this.login = login;
        this.fullYears = fullYears;
        this.servedInTheArmy = servedInTheArmy;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public Integer getFullYears() {
        return fullYears;
    }

    public void setFullYears(Integer fullYears) {
        this.fullYears = fullYears;
    }

    public Boolean getServedInTheArmy() {
        return servedInTheArmy;
    }

    public void setServedInTheArmy(Boolean servedInTheArmy) {
        this.servedInTheArmy = servedInTheArmy;
    }


    @Override
    public String toString() {
        return "Работник: " + "имя = " + name + ", фамилия = " + surname +
                ", 'полных' лет = " + fullYears + ", служба в армии = " + servedInTheArmy;
    }
}
