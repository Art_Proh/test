package by.prohor.entity.employee;

/**
 * Created by Artem Prokharau on 15.11.2020
 */

public interface EmployeeRepository<T> {

    public void hireEmployee(T obj);

    public void fireEmployee(String login);

}
