package by.prohor.entity.employee;

import by.prohor.repository.ConnectorRepository;
import lombok.extern.slf4j.Slf4j;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by Artem Prokharau on 17.11.2020
 */

@Slf4j
public class EmployeeRepositoryImpl extends ConnectorRepository implements EmployeeRepository<EmployeeImpl> {

//    public static final Logger logger = LoggerFactory.getLogger(LoggerUtils.getCurrentClassName());

    @Override
    public void hireEmployee(EmployeeImpl employeeImpl) {

        String requestSql = "INSERT INTO employee(name, surname, login, full_years, servedInTheArmy) " +
                "VALUES (?,?,?,?,?)";

        try (PreparedStatement statement = getConnection().prepareStatement(requestSql)) {
            statement.setString(1, employeeImpl.getName());
            statement.setString(2, employeeImpl.getSurname());
            statement.setString(3, employeeImpl.getLogin());
            statement.setInt(4, employeeImpl.getFullYears());
            statement.setBoolean(5, employeeImpl.getServedInTheArmy());

            statement.executeUpdate();

        } catch (SQLException ex) {
            log.error("Ошибка с базой данных => {}", ex.getMessage());
            ex.printStackTrace();
        }
    }

    @Override
    public void fireEmployee(String login) {

        String requestSql = "DELETE FROM employee WHERE login LIKE '" + login + "'";

        try (Statement statement = getConnection().createStatement()) {

            statement.executeUpdate(requestSql);

        } catch (SQLException ex) {
            log.error("Ошибка с базой данных => {}", ex.getMessage());
            ex.printStackTrace();
        }


    }
}
