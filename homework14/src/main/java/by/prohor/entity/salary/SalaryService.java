package by.prohor.entity.salary;

import by.prohor.entity.employee.EmployeeImpl;

/**
 * Created by Artem Prokharau on 15.11.2020
 */

public interface SalaryService {

    void addSalary(SalaryImpl salaryImpl, EmployeeImpl employeeImpl);


}
