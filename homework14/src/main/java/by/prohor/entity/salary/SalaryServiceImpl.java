package by.prohor.entity.salary;

import by.prohor.entity.employee.EmployeeImpl;
import by.prohor.logger.LoggerUtils;
import by.prohor.repository.ConnectorRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created by Artem Prokharau on 17.11.2020
 */

public class SalaryServiceImpl extends ConnectorRepository implements SalaryService {

    public static final Logger logger = LoggerFactory.getLogger(LoggerUtils.getCurrentClassName());


    @Override
    public void addSalary(SalaryImpl salaryImpl, EmployeeImpl employeeImpl) {

        String requestSql = "INSERT INTO salary (login_employee, current_rate, days_worked, current_salary, salary_usd, annual_growth)  VALUES (?,?,?,?,?,?)";


        try (PreparedStatement statement = getConnection().prepareStatement(requestSql)) {
            logger.debug("Подключился к базе и отправил запрос: {}", statement);

            statement.setString(1, employeeImpl.getLogin());
            statement.setDouble(2, salaryImpl.getCurrentRate());
            statement.setInt(3, salaryImpl.getDaysWorked());
            statement.setDouble(4, salaryImpl.getCurrentSalary());
            statement.setDouble(5, salaryImpl.getSalaryUsd());
            statement.setDouble(6, salaryImpl.getAnnualGrowth());
            statement.executeUpdate();
            logger.debug("Данные добавленны в базу");

        } catch (SQLException ex) {
            logger.error(ex.getMessage(), ex);
            ex.printStackTrace();
        }


    }
}
