package by.prohor.entity.salary;

import org.springframework.stereotype.Component;

/**
 * Created by Artem Prokharau on 15.11.2020
 */

@Component
public class SalaryImpl implements Salary {

    private Integer id;
    private Double currentRate;
    private Integer daysWorked;
    private Double currentSalary;
    private Double salaryUsd;
    private Double annualGrowth;

    public SalaryImpl() {
    }

    public SalaryImpl(Double currentRate, Integer daysWorked, Double currentSalary, Double salaryUsd, Double annualGrowth) {
        this.currentRate = currentRate;
        this.daysWorked = daysWorked;
        this.currentSalary = currentSalary;
        this.salaryUsd = salaryUsd;
        this.annualGrowth = annualGrowth;
    }

    public Double getCurrentRate() {
        return currentRate;
    }

    public void setCurrentRate(Double currentRate) {
        this.currentRate = currentRate;
    }

    public Integer getDaysWorked() {
        return daysWorked;
    }

    public void setDaysWorked(Integer daysWorked) {
        this.daysWorked = daysWorked;
    }

    public Double getCurrentSalary() {
        return currentSalary;
    }

    public void setCurrentSalary(Double currentSalary) {
        this.currentSalary = currentSalary;
    }

    public Double getSalaryUsd() {
        return salaryUsd;
    }

    public void setSalaryUsd(Double salaryUsd) {
        this.salaryUsd = salaryUsd;
    }

    public Double getAnnualGrowth() {
        return annualGrowth;
    }

    public void setAnnualGrowth(Double annualGrowth) {
        this.annualGrowth = annualGrowth;
    }

    @Override
    public String toString() {
        return "Salary{" +
                "currentRate=" + currentRate +
                ", daysWorked=" + daysWorked +
                ", currentSalary=" + currentSalary +
                ", salaryUsd=" + salaryUsd +
                ", annualGrowth=" + annualGrowth +
                '}';
    }
}
