package by.prohor.entity.position;

import by.prohor.entity.employee.EmployeeImpl;
import by.prohor.logger.LoggerUtils;
import by.prohor.repository.ConnectorRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by Artem Prokharau on 18.11.2020
 */


public class PositionRepositoryImpl extends ConnectorRepository implements PositionRepository {

    public static final Logger logger = LoggerFactory.getLogger(LoggerUtils.getCurrentClassName());


    @Override
    public void createPosition(EmployeeImpl employeeImpl, PositionImpl positionImpl) {
        String requestSql = "INSERT INTO position (login_employee, experience, education)  VALUES (?,?,?)";


        try (PreparedStatement statement = getConnection().prepareStatement(requestSql)) {
            logger.debug("Подключился к базе и отправил запрос: {}", statement);

            statement.setString(1, employeeImpl.getLogin());
            logger.trace("запись логина: {}", employeeImpl.getLogin());
            statement.setInt(2, positionImpl.getExperience());
            logger.trace("запись имени: {}", positionImpl.getExperience());
            statement.setString(3, positionImpl.getEducation());
            logger.trace("запись фамилии: {}", positionImpl.getEducation());
            statement.executeUpdate();
            logger.debug("Данные добавленны в базу");

        } catch (SQLException ex) {
            logger.error(ex.getMessage(), ex);
            ex.printStackTrace();
        }
    }

    @Override
    public void readPosition(String login) {
        String requestSql = "SELECT * FROM position WHERE login_employee LIKE '" + login + "'";


        try (Statement statement = getConnection().createStatement()) {
            logger.debug("Подключился к базе {}", statement);

            ResultSet resultSet = statement.executeQuery(requestSql);

            if (resultSet.next()) {
                String experience = resultSet.getString("experience");
                String education = resultSet.getString("education");

                System.out.println("У работника с логином '" + login + "' опыт => "
                        + experience + "образование => " + education);
            }

        } catch (SQLException ex) {
            logger.error(ex.getMessage(), ex);
            ex.printStackTrace();
        }
    }

    @Override
    public void updatePosition(String login, Integer experience, String education) {

        String requestSql = "UPDATE position  SET experience = ? ,education = ? " +
                "WHERE login_employee ='" + login + "'";

        try (PreparedStatement statement = getConnection().prepareStatement(requestSql)) {
            logger.debug("Подключился к базе для обнавления данных{}", statement);

            statement.setInt(1, experience);
            logger.trace("обновляем опыт в таблице : {}", experience);
            statement.setString(2, education);
            logger.trace("обновляем образование в таблице : {}", education);
            statement.executeUpdate();
            logger.debug("Данные в базе обновленны");

        } catch (SQLException ex) {
            logger.error(ex.getMessage(), ex);
            ex.printStackTrace();
        }
    }


    @Override
    public void deletePosition(String login) {

        String requestSql = "DELETE FROM position WHERE login_employee = '" + login + "'";

        try (Statement statement = getConnection().createStatement()) {
            logger.debug("Подключился к базе для удаления данных{}", statement);

            statement.executeUpdate(requestSql);
            logger.debug("Данные в базе обновленны");
        } catch (SQLException ex) {
            logger.error("Ошибка с базой данных => {}", ex.getMessage());
            ex.printStackTrace();
        }

    }
}

//    private PositionRepository repository;
//
//    public void findAllExperience(){
//       List<String> list = repository.findAllByName();
//       list.forEach(System.out::println);
//    }






