package by.prohor.entity.position;

import org.springframework.stereotype.Component;

/**
 * Created by Artem Prokharau on 15.11.2020
 */

@Component
//@Entity
public class PositionImpl implements Position {

    //    @Id
    private Integer id;
    private String loginEmployee;
    private Integer experience;
    private String education;


    public PositionImpl(Integer experience, String education) {
        this.experience = experience;
        this.education = education;
    }

    public PositionImpl() {
    }

    public String getLoginEmployee() {
        return loginEmployee;
    }

    public void setLoginEmployee(String loginEmployee) {
        this.loginEmployee = loginEmployee;
    }

    public Integer getExperience() {
        return experience;
    }

    public void setExperience(Integer experience) {
        this.experience = experience;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    @Override
    public String toString() {
        return "Position{" +
                "id=" + id +
                ", loginEmployee='" + loginEmployee + '\'' +
                ", experience=" + experience +
                ", education='" + education + '\'' +
                '}';
    }
}
