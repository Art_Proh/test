package by.prohor.entity.position;


import by.prohor.entity.employee.EmployeeImpl;

/**
 * Created by Artem Prokharau on 15.11.2020
 */

//@Repository
public interface PositionRepository {//extends CrudRepository<Employee,Integer> {

    void createPosition(EmployeeImpl employeeImpl, PositionImpl positionImpl);

    void readPosition(String login);

    void updatePosition(String login, Integer experience, String education);

    void deletePosition(String login);

//List<String> findAllByName();
}