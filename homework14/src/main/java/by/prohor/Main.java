package by.prohor;

import by.prohor.springconfig.SpringConfig;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Created by Artem Prokharau on 15.11.2020
 */
public class Main {
    public static void main(String[] args) {

        ApplicationContext context = new AnnotationConfigApplicationContext(SpringConfig.class);


        WorkCompany bean = context.getBean(WorkCompany.class);


        System.out.println(bean);


    }
}

