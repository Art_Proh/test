package by.prohor.springconfig;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * Created by Artem Prokharau on 18.11.2020
 */


@Configuration
@ComponentScan("by.prohor")
@PropertySource("classpath:employee.properties")
public class SpringConfig {

}
