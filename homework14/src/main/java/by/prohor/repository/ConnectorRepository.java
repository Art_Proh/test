package by.prohor.repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by Artem Prokharau on 17.11.2020
 */

public abstract class ConnectorRepository {


    private final String login = "prohor";
    private final String psw = "art375pro";
    private final String url = "jdbc:mysql://localhost:3306/company";


    public Connection getConnection() throws SQLException {
        return DriverManager.getConnection(url, login, psw);
    }
}

