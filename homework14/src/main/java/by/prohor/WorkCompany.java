package by.prohor;

import by.prohor.entity.employee.Employee;
import by.prohor.entity.position.Position;
import by.prohor.entity.salary.Salary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by Artem Prokharau on 18.11.2020
 */

@Component
public class WorkCompany {

    private Employee employee;
    private Position position;
    private Salary salary;

    @Autowired
    public WorkCompany(Employee employee, Position position, Salary salary) {
        this.employee = employee;
        this.position = position;
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "WorkCompany :" +
                "\n" + employee +
                ",\n" + position +
                ",\n" + salary;
    }
}
