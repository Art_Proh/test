USE company;

DROP TABLE IF EXISTS salaryImpl;
DROP TABLE IF EXISTS positionImpl;
DROP TABLE IF EXISTS employeeImpl;


CREATE TABLE employeeImpl
(
    id              INTEGER PRIMARY KEY AUTO_INCREMENT,
    name            VARCHAR(30)        not null,
    surname         VARCHAR(30)        not null,
    login           VARCHAR(50) UNIQUE not null,
    full_years      INTEGER            not null,
    servedInTheArmy BOOLEAN            not null,
    data            DATETIME DEFAULT CURRENT_TIMESTAMP,
    INDEX emp_ind (login)
);

CREATE TABLE positionImpl
(
    id             INTEGER PRIMARY KEY AUTO_INCREMENT,
    login_employee VARCHAR(50)  not null,
    experience     INTEGER      not null,
    education      VARCHAR(100) not null,
    data           DATETIME DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (login_employee) REFERENCES employeeImpl (login) ON DELETE CASCADE
);

CREATE TABLE salaryImpl
(
    id             INTEGER PRIMARY KEY AUTO_INCREMENT,
    login_employee VARCHAR(50) not null,
    current_rate   DOUBLE      not null,
    days_worked    INTEGER     not null,
    current_salary DOUBLE      not null,
    salary_usd     DOUBLE      not null,
    annual_growth  DOUBLE      not null,
    FOREIGN KEY (login_employee) REFERENCES company.employeeImpl (login)

);


